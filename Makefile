# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/02/28 12:09:57 by aulamber          #+#    #+#              #
#    Updated: 2015/03/01 21:54:13 by aulamber         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = game_2048

SRC = ft_2048.c \
		grid.c \
		game.c \
		game_algo/iter.c \
		game_algo/is_joignable.c \
		game_algo/check_case.c \
		game_algo/join_case.c \
		game_algo/move_to_block.c \
		game_algo/get_case_to_compare.c \
		game_algo/check.c \
		game_algo/move_possible.c \
		game_algo/game_algo.c \
		main.c

CC = gcc

CCFLAG = -Wall -Wextra -Werror -lncurses
OBJFLAG = -Wall -Wextra -Werror  -Ilibft/
OBJ = $(SRC:.c=.o)

RED = \033[33;31m
BLUE = \033[33;34m
GREEN = \033[33;32m
RESET = \033[0m

.PHONY: clean all re fclean libft

all: $(NAME)

$(NAME): $(OBJ)
	@$(CC) -o $(NAME) $(CCFLAG) $^
	@echo "[$(GREEN)Compilation $(BLUE)$(NAME) $(GREEN)ok$(RESET)]"


%.o: %.c
		@$(CC) -c -o $@ $(OBJFLAG) $^

clean:
	@rm -f $(OBJ)
	@echo "[$(RED)Supression des .o de $(BLUE)$(NAME) $(GREEN)ok$(RESET)]"

fclean: clean
		@rm -f $(NAME)
		@echo "[$(RED)Supression de $(BLUE)$(NAME) $(GREEN)ok$(RESET)]"

re: fclean all
