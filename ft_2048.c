/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_2048.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 12:05:53 by aulamber          #+#    #+#             */
/*   Updated: 2015/03/01 22:26:35 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game_2048.h"

void				end_window(void)
{
	endwin();
}

WINDOW				*start_win(t_ncurse_ctrl *ncurse)
{
	int				y;
	int				x;

	ncurse->win = initscr();
	noecho();
	curs_set(0);
	keypad(ncurse->win, TRUE);
	getmaxyx(ncurse->win, y, x);
	ncurse->size.x = x / (SIZE_GRID);
	ncurse->size.y = y / (SIZE_GRID);
	return (ncurse->win);
}

void print_end(t_global *glob)
{
	if (glob->game.is_finish == 1)
		mvprintw(glob->ncurse.size.y / 2, glob->ncurse.size.x / 2,
			"you win. score : %d", glob->game.score);
	else if (glob->game.is_finish == 2)
		mvprintw(glob->ncurse.size.y / 2, glob->ncurse.size.x / 2,
		"you loose. score : %d", glob->game.score);
	timeout(1000000);
	getch();
	timeout(100);
	glob->game.is_finish = 0;
	glob->game.win_value *= 2;
	refresh();
}

void				ft_2048(void)
{
	t_global		glob;
	int				touch;

	touch = 0;
	initialize_grid(&glob.game);
	start_win(&glob.ncurse);
	game_add_rand_num(&glob.game, 0);
	timeout(100);
	while (touch != 27)
	{
		touch = getch();
		clear();
		if (touch == KEY_UP)
			game_new_event(&glob.game, top);
		else if (touch == KEY_DOWN)
			game_new_event(&glob.game, bot);
		else if (touch == KEY_RIGHT)
			game_new_event(&glob.game, right);
		else if (touch == KEY_LEFT)
			game_new_event(&glob.game, left);
		if (!glob.game.is_finish)
			print_grid_graphic(&glob);
		else
			print_end(&glob);
	}
	end_window();
}
