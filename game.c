/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 15:03:49 by aulamber          #+#    #+#             */
/*   Updated: 2015/03/01 22:37:48 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game_2048.h"

void				print_nb(t_global *glob, int x, t_vector2d *box_size)
{
	int				y;
	t_vector2d		tmp;

	y = 0;
	tmp.y = (box_size->y * x) + (box_size->y / 2);
	while (y < SIZE_GRID)
	{
		tmp.x = (box_size->x * y) + (box_size->x / 2);
		if (glob->game.map[x][y])
			mvprintw(tmp.y, tmp.x, "%d", glob->game.map[x][y]);
		y++;
	}
}

void				print_grid_graphic(t_global *glob)
{
	int				x;
	t_vector2d		box_size;

	getmaxyx(glob->ncurse.win, glob->ncurse.size.y, glob->ncurse.size.x);
	clear();
	x = 0;
	box_size.x = glob->ncurse.size.x / (SIZE_GRID);
	box_size.y = (glob->ncurse.size.y - 1) / (SIZE_GRID);
	while (x < SIZE_GRID)
	{
		print_nb(glob, x, &box_size);
		mvvline(1, x * box_size.x, ACS_VLINE, glob->ncurse.size.y);
		mvhline(x * box_size.y, 1, ACS_HLINE, glob->ncurse.size.x);
		x++;
	}
	mvhline(1, 1, ACS_HLINE, glob->ncurse.size.x);
	box(glob->ncurse.win, ACS_VLINE, ACS_HLINE);
	mvhline(0, 0, ' ', glob->ncurse.size.x);
	mvprintw(0,0,"your score -> %d", glob->game.score);
}
