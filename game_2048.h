/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_2048.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 12:04:04 by aulamber          #+#    #+#             */
/*   Updated: 2015/03/01 22:42:40 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GAME_2048_H
# define GAME_2048_H
# include <ncurses.h>
# define SIZE_GRID 4
#define BOOL char
#include <unistd.h>

enum				e_const
{
	WIN_VALUE = 2048
};

enum				e_event
{
	right,
	left,
	top,
	bot
};

typedef struct		s_game
{
	int				map[SIZE_GRID][SIZE_GRID];
	int				max_value;
	enum e_event	event;
	int				is_finish;
	size_t			score;
	int				win_value;
}					t_game;

typedef struct		s_vector2d
{
	int				y;
	int				x;
}					t_vector2d;

typedef struct		s_ncurse_ctrl
{
	WINDOW			*win;
	t_vector2d		size;
}					t_ncurse_ctrl;

typedef struct		s_global
{
	t_ncurse_ctrl	ncurse;
	t_game			game;
}					t_global;

typedef void		(*t_callback)(t_game *game, t_vector2d *pose);

WINDOW				*start_win(t_ncurse_ctrl *ncurse);
BOOL				get_case_to_compar(t_game *a, t_vector2d *b, t_vector2d *c);
void				end_window(void);
void				print_grid(t_game *game);
void				initialize_grid(t_game *game);
int					**ft_start(int ***grid);
void				ft_2048(void);
void				print_grid_graphic(t_global *glob);
void				join_case(t_game *game,int *a, int *b);
BOOL				check_position(t_vector2d *pose);
void				game_add_rand_num(t_game *game, int occure);
void				game_iter(t_game *a, t_callback b, t_callback c);
BOOL				is_joingable(t_game *a, t_vector2d *b, t_vector2d *c);
void				move_to_block(t_game *game);
BOOL				move_possible(t_game *game, t_vector2d *pose);
void				check_turn(t_game *game);
void				game_new_event(t_game *game, enum e_event e);

#endif
