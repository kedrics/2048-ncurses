/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:07:44 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 22:34:17 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"

static void		check_win(t_game *game, t_vector2d *pose)
{
	if (game->map[pose->y][pose->x] == game->win_value)
	{
		game->is_finish = 1;
	}
}

static void		check_lose(t_game *game, t_vector2d *pose)
{
	if (game->map[pose->y][pose->x] == 0)
	{
		game->is_finish = 0;
	}
	else if (game->is_finish == 2
		&& move_possible(game, pose))
	{
		game->is_finish = 0;
	}
}

void			check_turn(t_game *game)
{
	game->is_finish = 0;
	game_iter(game, check_win, NULL);
	if (game->is_finish != 1)
	{
		game->is_finish = 2;
		game_iter(game, check_lose, NULL);
	}
}
