/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_case.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmancero <jmancero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:28:50 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 17:40:14 by jmancero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"

BOOL check_position(t_vector2d *pose)
{
	if (pose->x >= SIZE_GRID || pose->x < 0)
		return (FALSE);
	if (pose->y >= SIZE_GRID || pose->y < 0)
		return (FALSE);
	return (TRUE);
}
