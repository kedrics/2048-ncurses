/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   game_algo.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 18:08:51 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 22:42:32 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"
#include <stdlib.h>

static void		applique_event(t_game *game, t_vector2d *pose)
{
	t_vector2d tmp;

	if (get_case_to_compar(game, pose, &tmp))
	{
		if (is_joingable(game, pose, &tmp))
		{
			join_case(game, &game->map[pose->y][pose->x],
				&game->map[tmp.y][tmp.x]);
		}
	}
}

void			game_add_rand_num(t_game *game, int occure)
{
	t_vector2d	tmp;
	int			rand_nb;

	rand_nb = rand() % (SIZE_GRID * SIZE_GRID);
	tmp.y = rand_nb % SIZE_GRID;
	tmp.x = rand_nb / SIZE_GRID;
	if (occure == (SIZE_GRID * SIZE_GRID))
		return ;
	if (game->map[tmp.y][tmp.x] == 0)
		game->map[tmp.y][tmp.x] = (rand() % 2 == 0) ? 2 : 4;
	else
		game_add_rand_num(game, ++occure);
}

void			game_new_event(t_game *game, enum e_event e)
{
	game->event = e;
	clear();
	move_to_block(game);
	game_iter(game, applique_event, NULL);
	move_to_block(game);
	check_turn(game);
	if (game->is_finish == 0)
	{
		game_add_rand_num(game, 0);
	}
}
