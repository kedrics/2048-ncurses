/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_case_to_compare.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:44:02 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 21:31:52 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"

BOOL get_case_to_compar(t_game *game, t_vector2d *pose, t_vector2d *to_compare)
{
	to_compare->y = pose->y;
	to_compare->x = pose->x;
	if (game->event == right)
	{
		to_compare->x = pose->x - 1;
	}
	else if (game->event == left)
	{
		to_compare->x = pose->x + 1;
	}
	else if (game->event == top)
	{
		to_compare->y = pose->y - 1;
	}
	else if (game->event == bot)
	{
		to_compare->y = pose->y + 1;
	}
	return (check_position(to_compare));
}
