/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_joignable.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:47:51 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 21:21:09 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"

BOOL is_joingable(t_game *game, t_vector2d *pose, t_vector2d *to_compare)
{
	return (game->map[pose->y][pose->x]
	== game->map[to_compare->y][to_compare->x]
	|| game->map[to_compare->y][to_compare->x] == 0);
}
