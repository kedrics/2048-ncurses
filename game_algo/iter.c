/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iter.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 10:57:45 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 21:40:14 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"

static void game_iter_r(t_game *game, t_callback case_fnc, t_callback line_fnc)
{
	t_vector2d pose;

	pose.y = 0;
	while (pose.y < SIZE_GRID)
	{
		pose.x = SIZE_GRID - 1;
		while (pose.x >= 0)
		{
			if (case_fnc)
				case_fnc(game, &pose);
			pose.x--;
		}
		if (line_fnc)
			line_fnc(game, &pose);
	pose.y++;
	}
}

static void game_iter_b(t_game *game, t_callback case_fnc, t_callback line_fnc)
{
	t_vector2d pose;

	pose.x = 0;
	while (pose.x < SIZE_GRID)
	{
		pose.y = SIZE_GRID - 1;
		while (pose.y >= 0)
		{
			if (case_fnc)
				case_fnc(game, &pose);
			pose.y--;
		}
		if (line_fnc)
			line_fnc(game, &pose);
	pose.x++;
	}
}

static void game_iter_l(t_game *game, t_callback case_fnc, t_callback line_fnc)
{
	t_vector2d pose;

	pose.y = 0;
	while (pose.y < SIZE_GRID)
	{
		pose.x = 0 ;
		while (pose.x < SIZE_GRID)
		{
			if (case_fnc)
				case_fnc(game, &pose);
			pose.x++;
		}
		if (line_fnc)
			line_fnc(game, &pose);
	pose.y++;
	}
}

void game_iter(t_game *game,t_callback case_fnc, t_callback line_fnc)
{
	if (game->event == right)
	{
		game_iter_r(game, case_fnc, line_fnc);
	}
	else if (game->event == left) //
	{
		game_iter_l(game, case_fnc, line_fnc);
	}
	else if (game->event == top) //
	{
		game_iter_r(game, case_fnc, line_fnc);
	}
	else if (game->event == bot) //
	{
		game_iter_b(game, case_fnc, line_fnc);
	}
}
