/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   join_case.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:36:07 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 22:38:51 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"

void join_case(t_game *game,int *to_add, int *to_reset)
{
	if (*to_add == *to_reset)
		game->score += *to_add * 2;
	*to_add = *to_add + *to_reset;
	*to_reset = 0;
}
