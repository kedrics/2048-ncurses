/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_possible.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jmancero <jmancero@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 17:42:23 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 18:05:21 by jmancero         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"

BOOL move_possible(t_game *game, t_vector2d *pose)
{
	t_vector2d		tmp;
	int				i;
	enum e_event	tmp_e;

	i = 0;
	tmp_e = game->event;
	while (i < 4)
	{
		game->event = right + i;
		if (get_case_to_compar(game, pose, &tmp))
		{
			if (is_joingable(game, pose, &tmp))
			{
				game->event = tmp_e;
				return (TRUE);
			}
		}
		i++;
	}
	game->event = tmp_e;
	return (FALSE);
}
