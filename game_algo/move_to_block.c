/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move_to_block.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/01 14:53:10 by jmancero          #+#    #+#             */
/*   Updated: 2015/03/01 22:14:11 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../game_2048.h"

static void		move_to_right(t_game *game)
{
	t_vector2d	v;
	BOOL		modif;

	modif = 0;
	v.y = 0;
	while (v.y < SIZE_GRID)
	{
		v.x = 0;
		while (v.x < SIZE_GRID)
		{
			if (v.x + 1 < SIZE_GRID && game->map[v.y][v.x] != 0
				&& game->map[v.y][v.x + 1] == 0)
			{
				join_case(game, &game->map[v.y][v.x + 1], &game->map[v.y][v.x]);
				modif = 1;
				v.x = 0;
			}
			v.x++;
		}
		v.y++;
	}
	if (modif)
		move_to_right(game);
}

static void		move_to_left(t_game *game)
{
	t_vector2d	v;
	BOOL		modif;

	v.y = 0;
	modif = 0;
	while (v.y < SIZE_GRID)
	{
		v.x = SIZE_GRID - 1;
		while (v.x >= 0)
		{
			if (v.x - 1 >= 0 && game->map[v.y][v.x] != 0
				&& game->map[v.y][v.x - 1] == 0)
			{
				modif = 1;
				join_case(game, &game->map[v.y][v.x - 1], &game->map[v.y][v.x]);
				v.x = SIZE_GRID - 1;
			}
			v.x--;
		}
		v.y++;
	}
	if (modif)
		move_to_left(game);
}

static void		move_to_bot(t_game *game)
{
	t_vector2d	v;
	BOOL		modif;

	modif = 0;
	v.x = 0;
	while (v.x < SIZE_GRID)
	{
		v.y = 0;
		while (v.y < SIZE_GRID)
		{
			if (v.y + 1 < SIZE_GRID && game->map[v.y][v.x] != 0
				&& game->map[v.y + 1][v.x] == 0)
			{
				join_case(game, &game->map[v.y + 1][v.x], &game->map[v.y][v.x]);
				modif = 1;
				v.y = 0;
			}
			v.y++;
		}
		v.x++;
	}
	if (modif)
		move_to_bot(game);
}

static void		move_to_top(t_game *game)
{
	t_vector2d	v;
	BOOL		modif;

	v.x = 0;
	while (v.x < SIZE_GRID)
	{
		v.y = SIZE_GRID - 1;
		while (v.y >= 0)
		{
			if (v.y - 1 >= 0 && game->map[v.y][v.x] != 0
				&& game->map[v.y - 1][v.x] == 0)
			{
				join_case(game, &game->map[v.y - 1][v.x], &game->map[v.y][v.x]);
				modif = 1;
				v.y = SIZE_GRID - 1;
			}
			v.y--;
		}
		v.x++;
	}
	if (modif)
		move_to_top(game);
}

void			move_to_block(t_game *game)
{
	if (game->event == right)
	{
		move_to_right(game);
	}
	else if (game->event == left)
	{
		move_to_left(game);
	}
	else if (game->event == bot)
	{
		move_to_bot(game);
	}
	else if (game->event == top)
	{
		move_to_top(game);
	}
}
