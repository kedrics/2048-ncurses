/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   grid.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulamber <aulamber@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/02/28 17:44:09 by aulamber          #+#    #+#             */
/*   Updated: 2015/03/01 22:22:08 by aulamber         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "game_2048.h"

void				initialize_grid(t_game *game)
{
	int				y;
	int				x;

	y = 0;
	x = 0;
	game->is_finish = 0;
	game->score = 0;
	game->win_value = WIN_VALUE;
	while (y < SIZE_GRID)
	{
		while (x < SIZE_GRID)
		{
			game->map[y][x] = 0;
			x++;
		}
		x = 0;
		y++;
	}
}
